#include <stdlib.h>
#include <time.h>
#include <fstream>
#include <iostream>
using namespace std;

int main() {
    cout << "Generate data..." << endl;
    ofstream fout;
    const char *fname = "data.txt";
    fout.open(fname);

    int N = 200000000;
    int data = 0;
    srand((int)time(0));
    for (int i = 0; i < N; i++) {
        data = rand() % 10001;
        fout << data << " ";
    }

    cout << "Generate data done, saved in " << fname << " !" << endl;
    fout.close();

    return 0;
}
