#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>
using namespace std;

int main(int argc, char *argv[]) {
    cout << "Calculating..." << endl;
    fstream fin;
    fin.open(argv[1]);
    if (!fin.is_open()) {
        cout << "Can't open file, please use command like: "
                << "time -p ./CppStyle data-500k.txt";
        return 1;
    }

    int data = 0;
    vector<int> Buf;
    int N = 0;
    double tmp = 0;
    double mean = 0;
    while (fin >> data) {
        Buf.push_back(data);
        N++;
        tmp = (double)(N - 1) / N;
        mean = mean * tmp + (double)data / N;
    }

    sort(Buf.begin(), Buf.end());

    int Median = 0;
    int mid = N / 2;
    if ((mid % 2) == 0) {
        Median = Buf[mid];
    } else {
        Median = (Buf[mid - 1] + Buf[mid]) / 2;
    }

    cout << "number of elements = " << N << ", capacity = " << Buf.capacity()
            << ", median = " << Median << ", mean = " << mean << endl;

    return 0;
}
