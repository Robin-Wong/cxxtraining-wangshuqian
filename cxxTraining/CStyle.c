#include <stdio.h>
#include <stdlib.h>

int comp(const void* a, const void* b) {
    return ((*(int*)a - *(int*)b > 0) ? 1 : -1);
}

int main (int argc, char *argv[]) {
    printf("Calculating...\n");
    FILE *fp = fopen(argv[1], "r");
    if (!fp) {
        printf("Can't open file, please use command like: "
                "time -p ./CStyle data-500k.txt\n");
        return -1;
    }

    int data = 0;
    int N = 0;
    double tmp = 0;
    double mean = 0;
    int bufSize = 1024;
    int* Buf = (int*)malloc(bufSize * sizeof(int));
    int mfs = fscanf(fp, "%d", &data);
    while (mfs != EOF) {
        N++;
        tmp = (double)(N - 1) / N;
        mean = mean * tmp + (double)data / N;
        if (N < bufSize) {
            Buf[N-1] = data;
        } else {
            bufSize += bufSize;
            Buf = (int*)realloc(Buf, bufSize * sizeof(int));
            Buf[N-1] = data;
        }
        mfs = fscanf(fp, "%d", &data);
    }

    qsort(Buf, N, sizeof(Buf[0]), comp);

    int Median = 0;
    int mid = N / 2;
    if ((mid % 2) == 0) {
        Median = Buf[mid];
    } else {
        Median = (Buf[mid - 1] + Buf[mid]) / 2;
    }
    printf("number of elements = %d, capacity = %d, median = %d, mean = %.2lf\n",
            N, bufSize, Median, mean);
    fclose(fp);

    return 0;
}
